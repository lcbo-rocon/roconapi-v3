CREATE OR REPLACE PROCEDURE LCBO.GET_REFERENCEINFO_V3
(
 lv_sorOrderNumber IN NUMBER
 ,lv_lco_status IN VARCHAR
 ,ref_refinfo OUT SYS_REFCURSOR
)
IS
BEGIN
--Gold 3.0, RoconListener is using this SP for D status. Adding D status reference.
 IF lv_lco_status = 'W' THEN
 OPEN ref_refinfo FOR
 Select 
 REF_SYS1 as systemIdentifier
 ,REF_SYS1_NAME as systemName
 ,REF_SYS1_ID as referenceIdentifier
 ,REF_SYS1_TYPE as referenceIdentifierType
 ,REF_SYS1_SUBTYPE as referenceIdentifierSubtype
 ,REF_SYS1_COMM as referenceComment
 from LCBO.LCO_ORDER_REFERENCE
 WHERE status = 'W' and co_odno = lv_sorOrderNumber;
 ELSE
 OPEN ref_refinfo FOR
 Select 
 REF_SYS1 as systemIdentifier
 ,REF_SYS1_NAME as systemName
 ,REF_SYS1_ID as referenceIdentifier
 ,REF_SYS1_TYPE as referenceIdentifierType
 ,REF_SYS1_SUBTYPE as referenceIdentifierSubtype
 ,REF_SYS1_COMM as referenceComment
 from LCBO.LCO_ORDER_REFERENCE
 WHERE status = 'O' and co_odno = lv_sorOrderNumber
 union
 Select 
 REF_SYS2 as systemIdentifier
 ,REF_SYS2_NAME as systemName
 ,REF_SYS2_ID as referenceIdentifier
 ,REF_SYS2_TYPE as referenceIdentifierType
 ,REF_SYS2_SUBTYPE as referenceIdentifierSubtype
 ,REF_SYS2_COMM as referenceComment
 from LCBO.LCO_ORDER_REFERENCE
 WHERE status = 'O' and co_odno = lv_sorOrderNumber;
 
 END IF;
END;