CREATE OR REPLACE Procedure LCBO.GET_DELIVERY_DT_BY_ROUTE_V3
(
lv_order_date          IN VARCHAR2,
lv_number_of_available IN NUMBER,
--lv_route_code          IN LCBO.LCO_DELIVERY_SCHEDULE.DELIVERY_CODE%TYPE,
lv_customer			   IN VARCHAR2,
ref_delivery OUT SYS_REFCURSOR
)
IS

BEGIN

--Gold V3, do not allow customer to place order in the same week of delivery, except cancel status. 

OPEN ref_delivery FOR
SELECT DELIVERY_CODE AS ROUTE_CODE, ROWNUM, 
TO_CHAR(CUTOFF_DT,'YYYY-MM-DD HH24:MI') CUTOFF_DT, 
TO_CHAR(DELIVERY_DT,'YYYY-MM-DD HH24:MI') DELIVERY_DT
FROM (
	Select LDS.DELIVERY_CODE, CUTOFF_DT, DELIVERY_DT from LCBO.LCO_DELIVERY_SCHEDULE LDS 
	INNER JOIN LCBO.LCO_CUSTOMERS CUS ON LDS.DELIVERY_CODE=CUS.DELIVERY_CODE
	WHERE ltrim(rtrim(CUS.CU_NO)) = lv_customer 
	AND CUTOFF_DT >= TO_DATE(lv_order_date,'YYYY-MM-DD HH24:MI')
	AND TRUNC(DELIVERY_DT) NOT IN 
		(SELECT TRUNC(order_reqd_dt) FROM 
			(SELECT DISTINCT TRUNC(order_reqd_dt) order_reqd_dt FROM lcbo.V_ORDERS
			WHERE ltrim(rtrim(cu_no)) = lv_customer
			--AND CO_STATUS IN ('W', 'O')
			AND CO_STATUS <> 'D'
			ORDER BY order_reqd_dt DESC)
		--WHERE ROWNUM <=lv_number_of_available
		)
	ORDER BY CUTOFF_DT ASC
) WHERE ROWNUM <= lv_number_of_available;

END;