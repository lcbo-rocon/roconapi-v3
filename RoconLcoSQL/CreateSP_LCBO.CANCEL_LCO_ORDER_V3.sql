CREATE OR REPLACE PROCEDURE LCBO.CANCEL_LCO_ORDER_V3
(
    lv_orderNumber IN VARCHAR2
   ,lv_ReturnCode OUT NUMBER
    ,lv_returnCancel_DT_TM OUT DATE
    )
IS
    lv_rowsaffected NUMBER;
    lv_nextPoint NUMBER;
    lv_oStatus CHAR;
    eCnt NUMBER;
BEGIN
    lv_ReturnCode := 0;
    lv_nextPoint := 0;
    
   -- CHECK if order has been cancelled or Not W in ROCON.
    SELECT count(1) INTO eCnt FROM DBO.ON_COHDR WHERE CO_ODNO = rpad(lv_orderNumber,16);
    IF eCnt > 0 THEN
      SELECT ltrim(rtrim(CO_STATUS)) INTO lv_oStatus FROM DBO.ON_COHDR WHERE CO_ODNO = rpad(lv_orderNumber,16);
    ELSE
      lv_oStatus := 'X';
    END IF;
       IF lv_oStatus = 'D' THEN
           lv_ReturnCode := 1; -- Order is already cancelled.
           RETURN;
       END IF;
     IF lv_oStatus <> 'W' THEN
          lv_ReturnCode := 2;  -- Order not in W status to be cancelled.
         RETURN;
       END IF; 
   
    -- Update 'W' to 'D' in ON_COHDR table to cancel LCO order.
    IF lv_nextPoint = 0 THEN

       UPDATE DBO.ON_COHDR SET CO_STATUS = 'D'
       WHERE CO_ODNO = rpad(lv_OrderNumber,16) AND CO_STATUS = 'W';

        lv_rowsAffected := SQL%ROWCOUNT;
        IF lv_rowsAffected != 1 THEN
            lv_returnCode := 3;  -- Cannot update ON_COHDR table.
        ELSE
            lv_nextPoint := 1;
        END IF;

      -- timestamp is auto updated. Get cancel time
        SELECT TIMESTAMP INTO lv_returnCancel_DT_TM FROM DBO.ON_COHDR WHERE CO_ODNO = rpad(lv_orderNumber,16);
         
    END IF; -- end nextPoint = 0

END;