CREATE OR REPLACE PROCEDURE LCBO.GET_ORDER_DETAIL_BY_STATUS_V3
( lv_sorOrderNumber IN CHAR
 ,lv_lco_status IN CHAR
 ,lv_orderDetails OUT SYS_REFCURSOR
 )
 IS
 BEGIN
 OPEN lv_orderDetails FOR
SELECT COD_LINE as lineNumber
,item as sku
,quantity as quantity
,shippedQuantity as shippedQuantity
,agy_price as SellingPrice
,price as unitPrice
,deposit_price as itemBottleDeposit
,agy_discount as itemDiscountAmount
,0 as licMarkup
,agy_tax as hstTax
,retail_price as retailPrice
,agy_price * quantity as lineTotal
FROM LCBO.V_LCO_ORDER_PRICE_BREAKDOWN
WHERE co_odno = rpad(lv_sorOrderNumber,16)
and lco_status = lv_lco_status
 ORDER BY lineNumber ASC;
END GET_ORDER_DETAIL_BY_STATUS_V3;