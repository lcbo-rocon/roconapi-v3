TRIGGER LCBO.identity_LCO_ERR_LOG
before insert on lcbo.LCO_SERVICE_ERR_LOG
for each row
begin
  --- Get the next value  ---
  select LCBO.SEQ_LCO_ERR_LOG.NEXTVAL
  into :new.LOG_ID
  from dual;
end;