﻿using RoconApi.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoconApi.Services
{
    public interface IDeliveryChargeServices
    {
        Task<LCODeliveryCharge> GetDeliveryChargeByCuCaseAsync(string custNum, string caseNum, string orderDate);
      
    }
}
