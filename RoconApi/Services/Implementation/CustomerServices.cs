﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Dapper.Oracle;
using Dapper;
using AutoMapper;
using System.Collections.Generic;
using System;
using RoconApi.Domains;
using RoconApi.Utility;
using RoconApi.Models;


namespace RoconApi.Services.Implementation
{
    public class CustomerServices: ICustomerServices
    {
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public CustomerServices ( IConfiguration configuration, IMapper mapper)
        {
            _configuration = configuration;
            _mapper = mapper;
        }
        public IDbConnection Connection
        {
            get
            {
                return new OracleConnection(_configuration.GetConnectionString("roconconnection"));
            }
        }

        public async Task<IEnumerable<AvailableShippingDateEntity>> GetAvailableShippingDatesAsync(string customerNumber)
        {
            using (IDbConnection conn = Connection)
            {
                var param = new OracleDynamicParameters();
                param.Add("lv_order_date", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));
                param.Add("lv_number_of_available", 5);
                //param.Add("lv_route_code", routeCode);
                param.Add("lv_customer", customerNumber);
                param.Add("ref_delivery", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

                string storeProc = "LCBO.GET_DELIVERY_DT_BY_ROUTE_V3";

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {
                    var results = await conn.QueryAsync<AvailableShippingDateEntity>(storeProc, param: param, commandType: CommandType.StoredProcedure);
                    return results.ToArray();
                }
            }
            return null;
        }

        public async Task<Customer> GetCustomerByNumberAsync(string custNum)
        {
            using (IDbConnection conn = Connection)
            {  
                //need validation if routecode = 9999 or not returning delivery date, customer probably not setup as LCO yet.
                var param = new OracleDynamicParameters();
                param.Add("lv_customer", custNum);
                param.Add("ref_customer", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

                string storeProc = "LCBO.GET_CUSTOMER_INFO";

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                var newCustErrorInfo = new List<ErrorDetail>();

                if (conn.State == ConnectionState.Open)
                {
                    var results = await conn.QueryAsync<CustomerEntity, RouteInfoEntity, CustomerContactEntity,
                        ShipToInfoEntity, BillToInfoEntity,CustomerEntity>(storeProc, map: (cust, route, custc, ship,bill) =>
                        {
                            cust.routeInfo = route;
                            cust.customerContact = custc;
                            cust.shipToInfo = ship;
                            cust.billToInfo = bill;
                            return cust;
                        }, splitOn: "routeCode, contactName, shipToName, billToName", param: param, commandType: CommandType.StoredProcedure);

                    var customerDTO = results.SingleOrDefault();

                    
                    //customer is not found
                    if (customerDTO == null)
                    {
                        newCustErrorInfo.Add(new ErrorDetail()
                           {
                               errorType = ErrorType.OTHER.ToString(),
                               errorMessage = "Cannot find this customer " + custNum
                           }
                        );
                       return null;
                    }
                    else
                    {
                        if (customerDTO.customerStatus == "ACTIVE")
                        {
                            //Gold 3.0, using customer to get delivery_code to calculate delivery date. 
                            //var availDTO = await GetAvailableShippingDatesAsync(customerDTO.routeInfo.routeCode);
                            var availDTO = await GetAvailableShippingDatesAsync(custNum);
                            if (availDTO?.Any() == false)
                            {
                                newCustErrorInfo.Add(new ErrorDetail()
                                {
                                    errorType = ErrorType.INVALID_DATE.ToString(),
                                    errorMessage = "No available delivery dates for this customer " + custNum
                                }
                                );
                            }
                            else
                            {
                                customerDTO.availableShippingDates = availDTO.ToArray();
                            }
                        }
                    };
                    

                    var customer = _mapper.Map<Customer>(customerDTO);
                    if (newCustErrorInfo.Count > 0 )
                    {
                        customer.errorInfo = newCustErrorInfo.ToArray();
                    }
                    //return _mapper.Map<Customer>(customerDTO);
                    return customer;
                }
            }
                return null;
        }


        public async Task<IEnumerable<Customer>> GetAllCustomersAsync(string custType)
        {
            using (IDbConnection conn = Connection)
            {
                string custNum = custType;
                var param = new OracleDynamicParameters();
                param.Add("lv_customer", custNum);
                param.Add("ref_customer", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

                string storeProc = "LCBO.GET_CUSTOMER_INFO";

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {
                    var results = await conn.QueryAsync<CustomerEntity, RouteInfoEntity, CustomerContactEntity,
                       ShipToInfoEntity, BillToInfoEntity,  CustomerEntity>
                       (storeProc, map: (cust, route, custc, ship, bill) =>
                       {
                           cust.routeInfo = route;
                           cust.customerContact = custc;
                           cust.shipToInfo = ship;
                           cust.billToInfo = bill;
                           //cust.availableShippingDates = shipd;
                           return cust;
                       }, splitOn: "routeCode, contactName, shipToName, billToName", param: param, commandType: CommandType.StoredProcedure);

                    return _mapper.Map<IEnumerable<Customer>>(results.ToArray());
                }
            }
            return null;
        }


    }
}