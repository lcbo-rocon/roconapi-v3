﻿using AutoMapper;
using Dapper;
using Dapper.Oracle;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using RoconApi.Domains;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace RoconApi.Services.Implementation
{
    public class DeliveryChargeServices: IDeliveryChargeServices
    {
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public DeliveryChargeServices(IConfiguration configuration, IMapper mapper)
        {
            _configuration = configuration;
            _mapper = mapper;
        }
        public IDbConnection Connection
        {
            get
            {
                return new OracleConnection(_configuration.GetConnectionString("roconconnection"));
            }
        }
        public async Task<LCODeliveryCharge> GetDeliveryChargeByCuCaseAsync(string custNum, string orderCase, string orderDate)
        {
            using (IDbConnection conn = Connection)
            {
                //need validation if routecode = 9999 or not returning delivery date, customer probably not setup as LCO yet.
                var param = new OracleDynamicParameters();
                param.Add("lv_customer", custNum);
                param.Add("lv_orderCase", orderCase);
                param.Add("lv_orderDate", orderDate);
                param.Add("ref_deliverCharge", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

                string storeProc = "LCBO.GET_DELIVERY_CHARGE_BY_CU_CASE";

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {
                    var results = await conn.QueryAsync<CustomerEntity, RouteInfoEntity, CustomerContactEntity,
                        ShipToInfoEntity, BillToInfoEntity, CustomerEntity>(storeProc, map: (cust, route, custc, ship, bill) =>
                        {
                            cust.routeInfo = route;
                            cust.customerContact = custc;
                            cust.shipToInfo = ship;
                            cust.billToInfo = bill;
                            return cust;
                        }, splitOn: "routeCode, contactName, shipToName, billToName", param: param, commandType: CommandType.StoredProcedure);

                    var deliveryChargeDTO = results.SingleOrDefault();


                    //customer is not found
                    if (deliveryChargeDTO == null)
                    {
                        return null;
                    }

                    var customer = _mapper.Map<LCODeliveryCharge>(deliveryChargeDTO);

                    return _mapper.Map<LCODeliveryCharge>(deliveryChargeDTO);


                }
            }
            return null;
        }
    }
}
