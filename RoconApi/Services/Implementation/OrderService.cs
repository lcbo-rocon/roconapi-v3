﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using System.Linq;
using RoconApi.Domains;
using RoconApi.Utility;
using Microsoft.Extensions.Configuration;
using Dapper.Oracle;
using Dapper;
using AutoMapper;
using System.Collections.Generic;
using System;
using RoconApi;
using RoconApi.Models;
using System.Transactions;

namespace RoconApi.Services.Implementation
{
    public class OrderService : IOrderService
    {
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public OrderService(IConfiguration configuration, IMapper mapper)
        {
            _configuration = configuration;
            _mapper = mapper;
        }
        public IDbConnection Connection
        {
            get
            {
                return new OracleConnection(_configuration.GetConnectionString("roconconnection"));
            }
        }

    // Need to save Jason string
    // Silver 2.0, reserve inventory when WCS send order request
    public Task<Order> ReserveInventory(Order order, IEnumerable<ProductInventory> productList)
        {
            // using (var tran = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
           
            IDbConnection conn = Connection;
            conn.Open();

            //set up price info
            foreach (var curitem in order.orderItems)
            {
                var ProdInv = productList.Where(p => p.sku == curitem.sku).First();
                curitem.priceInfo = _mapper.Map<OrderItemPriceInfo>(ProdInv.priceInfo);
                curitem.item_category = ProdInv.item_category;
            }

            bool isSuccess = false;
            using (var tran = conn.BeginTransaction()) //Or however you get the connection
            {
                //Insert into dbo.LCO_Orders for new order
                string storeProc = "LCBO.RESERVE_LCO_ORDER_HEADER_V2";

                var param = new OracleDynamicParameters();
                param.Add("lv_lcoOrderNumber", order.orderHeader.lcoOrderNumber, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                param.Add("lv_orderRequestedDate", FormatUtils.ConvertToYYYYMMDDHHMISS(order.orderHeader.orderRequestDate), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                param.Add("lv_orderSource", order.orderHeader.orderSourceInfo.systemIdentifier, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                param.Add("lv_orderSourceName", order.orderHeader.orderSourceInfo.systemName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                param.Add("lv_overwriteShipToInformation", order.orderHeader.shipToInfo.overwriteShipToInformation, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                param.Add("lv_overwriteShipToAddress1", order.orderHeader.shipToInfo.addressLine1, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_overwriteShipToAddress2", order.orderHeader.shipToInfo.addressLine2, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_overwriteShipToCity", order.orderHeader.shipToInfo.city, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_overwriteShipToProvinceCode", order.orderHeader.shipToInfo.provinceCode, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_overwriteShipToPhoneNumber", order.orderHeader.shipToInfo.phoneNumber, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_overwriteShipToName", order.orderHeader.shipToInfo.name, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_overwriteShipToCountry", order.orderHeader.shipToInfo.country, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_overwriteShipToPostalCode", order.orderHeader.shipToInfo.postalCode, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_DeliveryType", order.orderHeader.deliveryType, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: false);
                param.Add("lv_routeCode", order.orderHeader.routeInfo.routeCode, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: false);
                param.Add("lv_routeStop", order.orderHeader.routeInfo.routeStops, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                //param.Add("lv_order_priority", DBNull.Value); // order.orderHeader.orderPriority);
                param.Add("lv_customerNumber", order.orderHeader.customerNumber, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                param.Add("lv_orderRequiredDate", FormatUtils.ConvertToYYYYMMDD(order.orderHeader.orderRequiredDate), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: false);
                param.Add("lv_orderShipmentDate", FormatUtils.ConvertToYYYYMMDD(order.orderHeader.orderShipmentDate), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: false);
                param.Add("lv_orderType", order.orderHeader.orderType, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: false);
                param.Add("lv_ShiptoName", order.orderHeader.shipToInfo.name, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_em_no", "LCOWOO", dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);

                //Silver V2.0 fields
                //param.Add("lv_orderStatus", "W", dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                param.Add("lv_lcoSysIdentifier", order.orderHeader.lcoOrderSystem.systemIdentifier, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                param.Add("lv_lcoSysName", order.orderHeader.lcoOrderSystem.systemName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);

                //param.Add("lv_sorSysIdentifier", order.orderHeader.sorSystem.systemIdentifier, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                //param.Add("lv_sorSysName", order.orderHeader.sorSystem.systemName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);

                //Silver V2.1 fields, to store # of Bags in PackingInvoiceComments.
                if (order.orderHeader.orderCommentsInfo == null)
                {
                    order.orderHeader.orderCommentsInfo = new OrderCommentsInfo(); 
                }
                if (order.orderHeader.orderCommentsInfo.PackingInvoiceComment == null)
                {
                    order.orderHeader.orderCommentsInfo.PackingInvoiceComment = new PackingInvoiceComment();
                }

                param.Add("lv_packingInvCommLine1", order.orderHeader.orderCommentsInfo.PackingInvoiceComment.TextLine1, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_packingInvCommLine2", order.orderHeader.orderCommentsInfo.PackingInvoiceComment.TextLine2, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_packingInvCommLine3", order.orderHeader.orderCommentsInfo.PackingInvoiceComment.TextLine3, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);

                if (order.orderHeader.referenceInfo != null)
                {
                    var refInfos = order.orderHeader.referenceInfo.ToArray();
                    int refNum = 1;
                    string refParm = "lv_ref";
                    foreach (var refInf in refInfos)
                    {
                        param.Add(refParm + refNum.ToString() + "_SysID", refInf.referenceSystem.systemIdentifier, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                        param.Add(refParm + refNum.ToString() + "_SysName", refInf.referenceSystem.systemName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);

                        //order# in diff system
                        param.Add(refParm + refNum.ToString() + "_Identifier", refInf.referenceIdentifier, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                        param.Add(refParm + refNum.ToString() + "_IdentifierType", refInf.referenceIdentifierType, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                        param.Add(refParm + refNum.ToString() + "_IdentifierSubtype", refInf.referenceIdentifierSubtype, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                        param.Add(refParm + refNum.ToString() + "_Comment", refInf.referenceComment, dbType: OracleMappingType.Varchar2, isNullable: true, direction: ParameterDirection.Input);
                        refNum = refNum + 1;
                    }
                }

                param.Add("lv_sorOrderNumber", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);
                
                try
                {
                    conn.Execute(storeProc, param: param, commandType: CommandType.StoredProcedure);

                    var newOrderNumber = param.Get<Decimal>("lv_sorOrderNumber");
                    
                    order.orderHeader.sorOrderNumber = Decimal.ToInt32(newOrderNumber);

                    if (order.orderHeader.sorSystem == null)
                    {
                        order.orderHeader.sorSystem = new SystemIdentify();
                        order.orderHeader.sorSystem.systemIdentifier = "R";
                        order.orderHeader.sorSystem.systemName = "R:ROCON";
                    }

                    order.orderHeader.orderSourceInfo.systemIdentifier = "R";
                    order.orderHeader.orderSourceInfo.systemName = "R:ROCON";
                    
                    //insert items to Rocon by SKU number so Rocon Product Invoice can order by SKU.
                    //linenumber in ROCON will be different than WCS orders
                    //OMS need WCS line # as key to check shortship qty. cannot change linenumber in Rocon. Change back to save order in WCS line #
                    //var sortedOrderItems = order.orderItems.OrderBy(p => p.sku);
                    var sortedOrderItems = order.orderItems.OrderBy(p => p.lineNumber);

                    //loop through items to secure the inventory
                    int orderedLineNumbers = 1;
                    foreach (var itm in sortedOrderItems.ToArray())
                    {
                        var paramList = new OracleDynamicParameters();
                        paramList.Add("lv_co_odno", order.orderHeader.sorOrderNumber.ToString(), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                        //paramList.Add("lv_cod_line", orderedLineNumbers, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                        paramList.Add("lv_cod_line", itm.lineNumber, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                        paramList.Add("lv_item", itm.sku.ToString(), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                        paramList.Add("lv_cod_qty", itm.quantity, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                        paramList.Add("lv_total_allocated", itm.quantity, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                        paramList.Add("lv_unit_price", itm.priceInfo.unit_price, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                        paramList.Add("lv_co_status", "W", dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                        paramList.Add("lv_em_no", "LCOWOO", dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                        paramList.Add("lv_rows_affected", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);

                        storeProc = "LCBO.RESERVE_LCO_ORDER_DETAILS";

                        conn.Execute(storeProc, param: paramList, commandType: CommandType.StoredProcedure);

                        var rowsaffected = Decimal.ToInt32(paramList.Get<Decimal>("lv_rows_affected"));

                        if (rowsaffected == 1)
                        {
                            orderedLineNumbers = orderedLineNumbers + 1;
                        }

                        //Sanity check that number of items have been inserted into ON_CODTL Table
                        if (orderedLineNumbers != order.orderItems.Count())
                        {
                            isSuccess = false;
                        }
                    }
                    tran.Commit();

                    //build orderHeader invoice totals for W status, to return back to WCS for WOOCommerce estimate use.
                    if (order.orderHeader.invoiceInfo == null)
                    {
                        order.orderHeader.invoiceInfo = new InvoiceInfo();
                    }

                    order.orderHeader.invoiceInfo.InvoiceNumber = order.orderHeader.sorOrderNumber.ToString();
                    order.orderHeader.invoiceInfo.InvoiceType = "E";
                    order.orderHeader.invoiceInfo.InvoiceTotalAmount = order.orderItems.Sum(p => p.priceInfo.selling_price * p.quantity);
                    order.orderHeader.invoiceInfo.InvoiceDiscountAmount = order.orderItems.Sum(p => p.priceInfo.discount * p.quantity);
                    order.orderHeader.invoiceInfo.InvoiceBottleDepositAmount = order.orderItems.Sum(p => p.priceInfo.bottle_deposit * p.quantity);
                    order.orderHeader.invoiceInfo.InvoiceHstAmount = order.orderItems.Sum(p => p.priceInfo.hst_tax * p.quantity);
                    order.orderHeader.invoiceInfo.InvoiceLicMuAmount = order.orderItems.Sum(p => p.priceInfo.liquor_markup * p.quantity);
                    order.orderHeader.invoiceInfo.InvoiceLevyAmount = 0m;
                    order.orderHeader.invoiceInfo.InvoiceRetailAmount = order.orderItems.Sum(p => p.priceInfo.retail_price * p.quantity);

                    if (order.orderHeader.invoiceInfo.DeliveryCharge == null)
                    {
                        order.orderHeader.invoiceInfo.DeliveryCharge = new DeliveryCharge();
                        order.orderHeader.invoiceInfo.DeliveryCharge.DeliveryBaseAmount = 0m;
                        order.orderHeader.invoiceInfo.DeliveryCharge.DeliveryHstAmount = 0m;

                        //TODO: When we implement the proper delivery charge structure this logic should be removed.
                        if (order.orderHeader.deliveryType == DeliveryType.SHIP_TO_ADDR_ON_FILE.ToString())
                        {
                            order.orderHeader.invoiceInfo.DeliveryCharge.DeliveryChargeType = DeliveryChargeType.CalculateDelivery.ToString();
                        }
                        else
                        {
                            order.orderHeader.invoiceInfo.DeliveryCharge.DeliveryChargeType = DeliveryChargeType.PickUp.ToString();
                            //after V2.1, below line will not be triggered. Need to replace when delivery change implemented.
                            if (order.orderHeader.orderCommentsInfo == null)
                            {
                                order.orderHeader.orderCommentsInfo = new OrderCommentsInfo()
                                {
                                    BolComment = "PICK UP ORDER"
                                };
                            }
                        }
                    }

                    isSuccess = true;
                }
                catch(OracleException ex)
                {
                    var sErrorList = new List<ErrorDetail>();

                    if (ex.Number == 1422)
                    {
                        sErrorList.Add(new ErrorDetail()
                        {
                            errorType = "INVALID_ORDER",
                            errorMessage = order.orderHeader.lcoOrderNumber + " already exist!",
                            errorData = new ErrorDetail.DataValue()
                            {
                                key = ErrorType.OTHER.ToString(),
                                value = order.orderHeader.lcoOrderNumber.ToString()
                            }
                        });
                    }
                    else
                    {
                        sErrorList.Add(new ErrorDetail()
                        {
                            errorType = "INVALID_ORDER",
                            errorMessage = "Unhandling exception occurred.",
                            errorData = new ErrorDetail.DataValue()
                            {
                                key = ErrorType.OTHER.ToString(),
                                value = ex.Message.ToString()
                            }
                        });
                    }
                    order.errorInfo = sErrorList.ToArray();
                }
                finally
                {
                    if (!isSuccess) tran.Rollback();
                }

               // if (!isSuccess) return null;
                return Task.FromResult(order);
            }
        }

    // Silver 2.0, change Order status to be O:Open when OMS send order request.
    public Task<Order> CreateOrder(Order order, IEnumerable<ProductInventory> productList)
        {
            ////build orderDetails table
            //foreach (var curitem in order.orderItems)
            //{
            //    var ProdInv = productList.Where(p => p.sku == curitem.sku).First();
            //    curitem.priceInfo = _mapper.Map<OrderItemPriceInfo>(ProdInv.priceInfo);
            //    curitem.item_category = ProdInv.item_category;
            //}

            //build orderHeader invoice totals
            if (order.orderHeader.invoiceInfo == null)
            {
                order.orderHeader.invoiceInfo = new InvoiceInfo();
            }

            order.orderHeader.invoiceInfo.InvoiceNumber = order.orderHeader.sorOrderNumber.ToString();
            //order.orderHeader.invoiceInfo.InvoiceType = "A"; //szl - shouldn't this be E instead of A, A is set when it is invoiced?
            order.orderHeader.invoiceInfo.InvoiceType = "E"; 
            order.orderHeader.invoiceInfo.InvoiceTotalAmount = order.orderItems.Sum(p => p.priceInfo.selling_price * p.quantity);
            order.orderHeader.invoiceInfo.InvoiceDiscountAmount = order.orderItems.Sum(p => p.priceInfo.discount * p.quantity);
            order.orderHeader.invoiceInfo.InvoiceBottleDepositAmount = order.orderItems.Sum(p => p.priceInfo.bottle_deposit * p.quantity);
            order.orderHeader.invoiceInfo.InvoiceHstAmount = order.orderItems.Sum(p => p.priceInfo.hst_tax * p.quantity);
            order.orderHeader.invoiceInfo.InvoiceLicMuAmount = order.orderItems.Sum(p => p.priceInfo.liquor_markup * p.quantity);
            order.orderHeader.invoiceInfo.InvoiceLevyAmount = 0m;
            order.orderHeader.invoiceInfo.InvoiceRetailAmount = order.orderItems.Sum(p => p.priceInfo.retail_price * p.quantity);


            if (order.orderHeader.invoiceInfo.DeliveryCharge == null)
            {
                order.orderHeader.invoiceInfo.DeliveryCharge = new DeliveryCharge();
                order.orderHeader.invoiceInfo.DeliveryCharge.DeliveryBaseAmount = 0m;
                order.orderHeader.invoiceInfo.DeliveryCharge.DeliveryHstAmount = 0m;

                //TODO: When we implement the proper delivery charge structure this logic should be removed.
                if (order.orderHeader.deliveryType == DeliveryType.SHIP_TO_ADDR_ON_FILE.ToString())
                {
                    order.orderHeader.invoiceInfo.DeliveryCharge.DeliveryChargeType = DeliveryChargeType.CalculateDelivery.ToString();
                }
                else
                {
                    order.orderHeader.invoiceInfo.DeliveryCharge.DeliveryChargeType = DeliveryChargeType.PickUp.ToString();
                    if (order.orderHeader.orderCommentsInfo == null)
                    {
                        order.orderHeader.orderCommentsInfo = new OrderCommentsInfo()
                        {
                            BolComment = "PICK UP ORDER"
                        };
                    }
                }
            }

            //ok now we start writing to the tables
            IDbConnection conn = Connection;
            conn.Open();
            bool isSuccess = false;
            int nextInvNo = 0;
            isSuccess = true;
            using (var tran = conn.BeginTransaction())
            {
                var param = new OracleDynamicParameters();
                param.Add("lv_nextInvNo", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);

                string storeProc = "LCBO.GET_NEXT_INVOICE_NO";

                try
                {
                    conn.Execute(storeProc, param: param, commandType: CommandType.StoredProcedure);
                    nextInvNo = Decimal.ToInt32(param.Get<Decimal>("lv_nextInvNo"));
                    //No rolling back as this can impact other users getting an inv_no from ROCON

                    tran.Commit();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    isSuccess = false;
                    var sErrorList = new List<ErrorDetail>();

                    sErrorList.Add(new ErrorDetail()
                    {
                        errorType = "INVALID_ORDER",
                        errorMessage = "ERROR Generating Invoice Number",
                        errorData = new ErrorDetail.DataValue()
                        {
                            key = ErrorType.OTHER.ToString(),
                            value = ex.Message.ToString()
                        }
                    });
                }
                finally
                {
                    //report error
                }
            }

            if (isSuccess)
            {
                // conn.Open();
                using (var tran = conn.BeginTransaction())
                {

                    string storeProc = "LCBO.COMMIT_NEW_ORDER_V2";

                    if (order.orderHeader.orderStatus == null)
                    {
                        order.orderHeader.orderStatus = new OrderStatusCodeDetail
                        {
                            orderStatusCode = "O",
                            orderStatusName = "O:OPEN"
                        };
                    }

                    var parmlist = new OracleDynamicParameters();
                    parmlist.Add("lv_invNumber", nextInvNo, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                    parmlist.Add("lv_orderNumber", order.orderHeader.sorOrderNumber, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    parmlist.Add("lv_OrderType", order.orderHeader.orderType, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    parmlist.Add("lv_startStage", 0, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                    parmlist.Add("lv_invoiceAmount", order.orderHeader.invoiceInfo.InvoiceTotalAmount, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                    parmlist.Add("lv_deliveryCharge", order.orderHeader.invoiceInfo.DeliveryCharge.DeliveryBaseAmount, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                    parmlist.Add("lv_deliveryTax1", 0m, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                    parmlist.Add("lv_deliveryTax2", order.orderHeader.invoiceInfo.DeliveryCharge.DeliveryHstAmount, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                    parmlist.Add("lv_fullCases", 0, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                    parmlist.Add("lv_partCases", 0, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                    parmlist.Add("lv_ReturnCode", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);

                    //--Silver V2.0 new fields
                    parmlist.Add("lv_orderSource", order.orderHeader.orderSourceInfo.systemIdentifier, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                    parmlist.Add("lv_orderSourceName", order.orderHeader.orderSourceInfo.systemName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);

                    parmlist.Add("lv_orderStatusCode", order.orderHeader.orderStatus.orderStatusCode, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                    parmlist.Add("lv_orderStatusName", order.orderHeader.orderStatus.orderStatusName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    //--Will not use statusTime value from other system
                    //parmlist.Add("lv_status_DT_TM", FormatUtils.ConvertToYYYYMMDDHHMISS(order.orderHeader.orderStatus.statusCreateTime), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    

                    parmlist.Add("lv_returnCreation_DT_TM", dbType: OracleMappingType.Date, direction: ParameterDirection.Output);

                    //--lco and sor ref should not change during order's life cycle. Info were saved to LCO_ORDER_REF table during 'W' status.
                    //parmlist.Add("lv_lcoSysIdentifier", order.orderHeader.lcoOrderSystem.systemIdentifier, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                    //parmlist.Add("lv_lcoSysName", order.orderHeader.lcoOrderSystem.systemName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    //parmlist.Add("lv_sorSysIdentifier", order.orderHeader.sorSystem.systemIdentifier, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                    //parmlist.Add("lv_sorSysName", order.orderHeader.sorSystem.systemName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);

                    if (order.orderHeader.referenceInfo != null)
                    {
                        var refInfos = order.orderHeader.referenceInfo.ToArray();
                        int refNum = 1;
                        string refParm = "lv_ref";
                        foreach (var refInf in refInfos)
                        {
                            parmlist.Add(refParm + refNum.ToString() + "_SysID", refInf.referenceSystem.systemIdentifier, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                            parmlist.Add(refParm + refNum.ToString() + "_SysName", refInf.referenceSystem.systemName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);

                            //order# in diff system
                            parmlist.Add(refParm + refNum.ToString() + "_Identifier", refInf.referenceIdentifier, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                            parmlist.Add(refParm + refNum.ToString() + "_IdentifierType", refInf.referenceIdentifierType, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                            parmlist.Add(refParm + refNum.ToString() + "_IdentifierSubtype", refInf.referenceIdentifierSubtype, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                            parmlist.Add(refParm + refNum.ToString() + "_Comment", refInf.referenceComment, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                            refNum = refNum + 1;
                        }
                    }
                    try
                    {
                        conn.Execute(storeProc, param: parmlist, commandType: CommandType.StoredProcedure);

                        var returnCode = Decimal.ToInt32(parmlist.Get<Decimal>("lv_ReturnCode"));
                        if (returnCode == 0)
                        {
                            tran.Commit();
                            isSuccess = true;
                            var returnCreationTime = FormatUtils.ConvertToYYYYMMDDHHMISS(parmlist.Get<DateTime>("lv_returnCreation_DT_TM").ToString());
                            order.orderHeader.orderCreationDate = returnCreationTime;
                            //Change orderstatus.creationtime to be ROCON O time.
                            order.orderHeader.orderStatus.statusCreateTime = returnCreationTime;
                        }
                        else
                        {
                            var sb = new System.Text.StringBuilder();
                            string errTable = "";
                            switch (returnCode)
                            {
                                case 1:
                                    errTable = "DBO.ON_INVOICE";
                                    break;
                                case 2:
                                    errTable = "DBO.ON_INV_CO";
                                    break;
                                case 3:
                                    errTable = "DBO.ON_INV_CODTL";
                                    break;
                                case 4:
                                    errTable = "DBO.ON_INV_CODTL_RULE";
                                    break;
                                case 5:
                                    errTable = "DBO.ON_COHDR (ORDER NOT ABLE TO BE O)";
                                    break;
                                case 6:
                                    errTable = "DBO.ON_COHDR (ORDER ALREADY IN CANCEL STATUS)";
                                    break;
                                case 7:
                                    errTable = "DBO.ON_COHDR (ORDER NOT IN W STATUS)";
                                    break;
                                default:
                                    errTable = "Unknown";
                                    break;
                            }
                            sb.AppendLine("Store Proc error: LCBO.COMMIT_NEW_ORDER_V2");
                            sb.AppendLine("Return Code:" + returnCode);
                            sb.AppendLine("ERROR writing to " + errTable);
                            throw new Exception(sb.ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        isSuccess = false;

                        var sErrorList = new List<ErrorDetail>();

                        sErrorList.Add(new ErrorDetail()
                        {
                            errorType = "INVALID_ORDER",
                            errorMessage = "Order " + order.orderHeader.lcoOrderNumber + " status cannot be changed to OPEN!",
                            errorData = new ErrorDetail.DataValue()
                            {
                                key = ErrorType.OTHER.ToString(),
                                value = ex.Message.ToString()
                            }
                        });
                        order.errorInfo = sErrorList.ToArray();
                    }
                    finally
                    {
                        if (!isSuccess) tran.Rollback();
                    }
                }
            }
            return Task.FromResult(order);
        }

    // Gold 3.0. WCS can call RoconAPI to Cancel order in 'W' status
    public Task<Order> CancelOrder(Order order)
        {
            IDbConnection conn = Connection;
            conn.Open();
            bool isSuccess = false;
            isSuccess = true;
            using (var tran = conn.BeginTransaction())
            {
                string storeProc = "LCBO.CANCEL_LCO_ORDER_V3";
                
                var param = new OracleDynamicParameters();
                param.Add("lv_orderNumber", order.orderHeader.sorOrderNumber, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                param.Add("lv_ReturnCode", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);
                param.Add("lv_returnCancel_DT_TM", dbType: OracleMappingType.Date, direction: ParameterDirection.Output);

                try
                {
                    conn.Execute(storeProc, param: param, commandType: CommandType.StoredProcedure);
                    var returnCode = Decimal.ToInt32(param.Get<Decimal>("lv_ReturnCode"));
                    
                    if (returnCode == 0)
                    {
                        tran.Commit();
                        var returnCancelTime = FormatUtils.ConvertToYYYYMMDDHHMISS(param.Get<DateTime>("lv_returnCancel_DT_TM").ToString());
                        if (order.orderHeader.orderStatus == null)
                        {
                            order.orderHeader.orderStatus = new OrderStatusCodeDetail
                            {
                                orderStatusCode = "D",
                                orderStatusName = "D:DELETED_CANCELLED",
                                statusCreateTime = returnCancelTime
                        };
                        }
                    }
                    else
                    {
                        isSuccess = false;
                        var sb = new System.Text.StringBuilder();
                        string errMsg = "";
                        switch (returnCode)
                        {
                            case 1:
                                errMsg = "Order is already cancelled.";
                                break;
                            case 2:
                                errMsg = "Order not in W status to be cancelled";
                                break;
                            case 3:
                                errMsg = "Cannot update ON_COHDR table";
                                break;
                            default:
                                errMsg = "Unknown";
                                break;
                        }
                        sb.AppendLine("Store Proc error: LCBO.CANCEL_LCO_ORDER" + " | " + "Return Code:" + returnCode + " | " + errMsg );
                        //sb.AppendLine("Return Code:" + returnCode);
                        //sb.AppendLine("ERROR writing to " + errMsg);
                        throw new Exception(sb.ToString());
                    }
                }
                catch (Exception ex)
                {
                    isSuccess = false;
                    var sErrorList = new List<ErrorDetail>();
                    sErrorList.Add(new ErrorDetail()
                    {
                        errorType = "ACTION_FAILED",
                        errorMessage = "ERROR on Cancelling order " + order.orderHeader.sorOrderNumber,
                        errorData = new ErrorDetail.DataValue()
                        {
                            key = ErrorType.ACTION_FAILED.ToString(),
                            value = ex.Message.ToString()
                        }
                    });
                    order.errorInfo = sErrorList.ToArray();
                }
                finally
                {
                    if (!isSuccess) tran.Rollback();
                }
            }
            return Task.FromResult(order);
        }

        public async Task<Order> GetOrderByIdAsync(string orderID, string orderInvType)
        {
            using (IDbConnection conn = Connection)
            {
                string RocOrderID = orderID;
                //string OrderInvType = "E"; //use 'A' if order is already invoiced

                //if orderID is lcoOrderNumber use GET_ORDER_INFO_BY_REF instead
                string storeProc = "LCBO.GET_ORDER_INFO_V2";

                var param = new OracleDynamicParameters();
                param.Add("lv_sorOrderNumber", RocOrderID);
                param.Add("lv_co_inv_type", orderInvType);
                param.Add("lv_order", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

                string sQuery = storeProc;

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {
                    //  var results = await conn.QueryAsync<OrderHeaderEntity>(sQuery, param: param, commandType: CommandType.StoredProcedure);
                    // Not returning OrderSourceInfo, OrderEvent, OrderStatus, PublishedStatus, ReferenceInfo, lcoSystem, sorSystem,
                    var results = await conn.QueryAsync<OrderHeaderEntity, RouteInfoEntity, OrderShipToInfoEntity,
                        OrderCommentsInfoEntity, PackingInvoiceCommentEntity, InvoiceInfoEntity, DeliveryChargeEntity,
                        OrderHeaderEntity>(sQuery, map: (header, route, ship, comment, packing, inv, delivery) =>
                         {
                             header.routeInfo = route;
                             header.shipToInfo = ship;
                             if (comment != null)
                             { 
                                 header.orderCommentsInfo = comment;
                             }

                             if (packing != null)
                             {
                                 header.orderCommentsInfo.PackingInvoiceComment = packing;
                             }

                             header.invoiceInfo = inv;
                             header.invoiceInfo.DeliveryCharge = delivery;
                             return header;
                         }
                            , splitOn: "routeCode,name,bolcomment,textLine1,invoiceNumber,deliveryChargeType"
                            , param: param, commandType: CommandType.StoredProcedure);

                    var order = new OrderEntity();

                    var orderHeaderInfo = results.SingleOrDefault();

                    order.orderHeader = orderHeaderInfo;

                   // Not returning shippedQTY
                    var orderItems = await GetOrderItemDetails(orderID);

                    order.orderItems = orderItems.ToArray();

                    return _mapper.Map<Order>(order);

                };

            }

            return null; //otherwise return null
        }

        public async Task<IEnumerable<OrderItemsEntity>> GetOrderItemDetails(string orderID)
        {
            using (IDbConnection conn = Connection)
            {
                string RocOrderID = orderID;
                string OrderInvType = "E"; //use 'A' if order is already invoiced

                //if orderID is lcoOrderNumber use GET_ORDER_INFO_BY_REF instead
                string storeProc = "LCBO.GET_ORDER_DETAIL_INFO_V2";

                var param = new OracleDynamicParameters();
                param.Add("lv_sorOrderNumber", RocOrderID);
                param.Add("lv_co_inv_type", OrderInvType);
                param.Add("lv_orderdetails", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

                string sQuery = storeProc;

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {
                    var results = await conn.QueryAsync<OrderItemsEntity, OrderItemPriceInfoEntity, OrderItemsEntity>(sQuery, map: (item, price) =>
                       {
                           item.priceinfo = price;
                           return item;
                       }, splitOn: "sellingPrice", param: param, commandType: CommandType.StoredProcedure);


                    return results.ToArray();
                }
            }
            return null;
        }
  
        //Get order history status in sequence
        public async Task<IEnumerable<OrderStatusDetailEntity>> GetOrderPublishedStatus(string orderID)
        {
            using (IDbConnection conn = Connection)
            {
                string RocOrderID = orderID;
                string storeProc = "LCBO.GET_ORDER_PUBLISHED_STATUS";

                var param = new OracleDynamicParameters();
                param.Add("lv_sorOrderNumber", RocOrderID);
                param.Add("ref_orderPublishedStatus", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {
                    var results = await conn.QueryAsync<OrderStatusDetailEntity>(storeProc, param: param, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            return null;

        }

        //Get all possible order status life cycle path
        public async Task<IEnumerable<OrderStatusDetailEntity>> GetSystemOrderStatusCycle()
        {
            using (IDbConnection conn = Connection)
            {
                string storeProc = "LCBO.GET_ORDER_STATUS_CYCLE";

                var param = new OracleDynamicParameters();
                param.Add("lv_orderStatusCycle", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {
                    var results = await conn.QueryAsync<OrderStatusDetailEntity>(storeProc, param: param, commandType: CommandType.StoredProcedure);
                    return results.ToArray();
                }
            }
            return null;

        }


    }
}
 