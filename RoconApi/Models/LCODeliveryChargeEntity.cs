﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace RoconApi.Models
{
    [DataContract]
    public class LCODeliveryChargeEntity
    {
            [DataMember(Name = "customerNumber")]
            public string customerNumber { get; set; }

            [DataMember(Name = "orderCases")]
            public string orderCases { get; set; }

            [DataMember(Name = "orderDate")]
            public string orderDate { get; set; }

            [DataMember(Name = "deliveryChargeAmnt")]
            public decimal deliveryChargeAmnt { get; set; }
        }
    }
}
