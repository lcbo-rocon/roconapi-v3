﻿using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;

namespace RoconApi.Domains
{
    [DataContract]
    [Serializable]
    public class LCODeliveryCharge: Resource
    {
        [DataMember(Name = "customerNumber")]
        [JsonProperty("customerNumber")]
        public string customerNumber { get; set; }

        [DataMember(Name = "orderCases")]
        [JsonProperty("orderCases")]
        public string orderCases { get; set; }

        [DataMember(Name = "orderDate")]
        [JsonProperty("orderDate")]
        public string orderDate { get; set; }

        [DataMember(Name = "deliveryChargeAmnt")]
        [JsonProperty("deliveryChargeAmnt")]
        public decimal deliveryChargeAmnt { get; set; }
    }
}
