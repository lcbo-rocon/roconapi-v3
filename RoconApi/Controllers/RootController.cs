﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using RoconApi.Domains;
using RoconApi.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoconApi.Controllers
{
    [ApiVersion("3.0")]
    [Route("/")]
    [ApiController]
    public class RootController : ControllerBase
    {
        [HttpGet(Name = nameof(GetRoot))]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public IActionResult GetRoot()
        {
            //In Gold V3.0, remove bearer token. Use Kong API Gateway.
            //if (Environment.GetEnvironmentVariable("CHECKSOUP") == "1")
            //{
            //    var bToken = Request.Headers["AUTHORIZATiON"];
            //    if (!Authorization.isAuthorized(bToken.ToString()))
            //    {
            //        return Unauthorized();
            //    }
            //}

            var response = new RootResponse
            {
                Self = Link.To(nameof(GetRoot)),
                Products = Link.ToCollection(nameof(ItemsController.GetProducts)),
                Customers = Link.ToCollection(nameof(CustomersController.GetAllCustomers))
               // Orders = Link.To(nameof(OrdersController.GetOrderByID), null)
                //Info = Link.To(nameof(InfoController.GetInfo), null),
                //Prices = Link.ToCollection(nameof(ItemsController.GetAllPrices), null)

            };

            return Ok(response);
        }

    }
}
