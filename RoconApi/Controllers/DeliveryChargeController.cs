﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RoconApi.Services;
using RoconApi.Domains;
using RoconApi.Utility;

namespace RoconApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeliveryChargeController : ControllerBase
    {

        private readonly IDeliveryChargeServices _deliveryChargeServices;
        private readonly IConfigurationProvider _mappingConfiguration;
        public DeliveryChargeController(IDeliveryChargeServices deliveryChargeServices, IConfigurationProvider mappingConfiguration)
        {
            _deliveryChargeServices = deliveryChargeServices;
            _mappingConfiguration = mappingConfiguration;
        }

        //GET /customers
        [HttpGet(Name = nameof(GetDeliveryChargeByCuCase))]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<LCODeliveryCharge>> GetDeliveryChargeByCuCase(string custNum, string caseNum, string orderDate)
        {
            
            var cuNum = custNum;
            var caNum = caseNum;
            var oDate = orderDate;
            var data = await _deliveryChargeServices.GetDeliveryChargeByCuCaseAsync(cuNum, caNum, oDate);
            if (data == null) return NotFound();

            var mapper = _mappingConfiguration.CreateMapper();
            var dc = mapper.Map<LCODeliveryCharge>(data);

            //add validation before sending back 
            return dc;
        }
    }
}